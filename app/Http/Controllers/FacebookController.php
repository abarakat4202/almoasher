<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;

class FacebookController extends Controller
{

    public function getLogin()
    {
        return view('login');
    }
    
    
    public function getPages() 
    {
        $fb = new Facebook(config('facebook.config'));
        $request = request();
        // retrieve form input parameters
        $uid = $request->uid;
        $access_token = $request->access_token;
        $permissions = $request->permissions; 

        // get long term access token for future use
        $oAuth2Client = $fb->getOAuth2Client();
        try
        {
            // assuming access_token field to exist in users table in database
            $access_token = $oAuth2Client->getLongLivedAccessToken($access_token)->getValue();
        }
        catch(FacebookResponseException $e)
        {
            return redirect(route('getLogin'));
        }    
        //store token to cache
        \Cache::put('fb_token',$access_token,1440);
        
        // set default access token for all future requests to Facebook API            
        $fb->setDefaultAccessToken($access_token);

        // call api to retrieve person's public_profile details
        $pages = $fb->get('/me/accounts')->getBody();
        $pages = json_decode($pages)->data;
        
        $data = new \stdClass;
        $data->pages = $pages;
    
        return view('pages',compact('data'));
    }        

    function getPageData()
    {
        //get page id from request
        if(!\Request::has('page_id'))
        {
            $data = new \stdClass;
            $data->status = 'fail';
            $data->code = 400;
            $data->message = 'no page id';
            return \Response::json($data,400);            
        }
        $fb = new Facebook(config('facebook.config'));
        $page_id = request()->page_id;
        

        // set default access token for all future requests to Facebook API            
        $fb->setDefaultAccessToken( \Cache::get('fb_token'));  
        
        //get page posts 
        $posts = $fb->get('/'.$page_id.'/posts?fields=created_time,attachments{media},message')->getBody();
        $posts = json_decode($posts)->data;
        $data = new \stdClass;
        $data->data =  $posts;
        $data->status = 'success';

        return \Response::json($data,200);  

    }

    public function createPost(Request $request)
    {
        $fb = new Facebook(config('facebook.config'));

        //validate the request
        $rules = [
            'message' => 'required',
            'image' => 'required|image|max:2000',
            'page_id' => 'required',
            'page_token' => 'required',
        ];

        $validator = \Validator::make($request->all(), $rules);
        //$this->validate($request, $rules);
        //get page id from request
        if($validator->fails())
        {
            $data = new \stdClass;
            $data->status = 'fail';
            $data->code = 422;
            $data->errors = $validator->errors();
            return \Response::json($data,422);            
        }

        $params = [];
        $params['caption'] = $request->message;
        $params['published'] = true;
        $image = $request->file('image');
        $fullName = $image->getClientOriginalName();
        $filename = pathinfo($fullName, PATHINFO_FILENAME);
        $extension = pathinfo($fullName, PATHINFO_EXTENSION);
        $imageName = $filename.'-'.time().'.'.$extension;            
        
        

        //upload image and get it's url

        //$image->storeAs('', $imageName, 'facebook');
        \Storage::disk('facebook')->putFileAs('', $image, $imageName);
        $imageUrl = \Storage::disk('facebook')->url($imageName);
        $params['url'] = $imageUrl;

        try{
        // set page access token          
        $fb->setDefaultAccessToken($request->page_token);          
        $post = $fb->post('/'.$request->page_id.'/photos', $params)->getBody();
        }
        catch(FacebookResponseException $e)
        {
            $data = new \stdClass;
            $data->status = 'fail';
            $data->code = 422;
            $data->errors = $validator->errors();
            return \Response::json($data,422);            
        }    
        $data = new \stdClass;
        $data->id =  $post->getBody();
        $data->status = 'success';        
        $data->local_url = $imageUrl;        
        return $data;
    }

}
