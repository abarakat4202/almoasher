@extends('main')
@section('content')
<div class="row justify-content-center" style="margin-top:20px;">
    <button id="btn-login" type="button" class="btn btn-primary btn-lg">
        <span> Login with Facebook</span>
    </button> 
</div>
@endsection
@section('js')
<script>
var csrf_token = '{{ csrf_token() }}';
var postUrl = '{{ route('postLogin') }}';
$(document).ready(function() {
    $.ajaxSetup({ cache: true }); 
    $.getScript('//connect.facebook.net/en_US/sdk.js', function () {
        // initialize facebook sdk
        FB.init({
            appId: "{{ config('facebook.config.app_id') }}", // app id
            status: true,
            cookie: true,
            version: 'v3.1'
        });

        // attach login click event handler
        $("#btn-login").click(function(){
            $(this).addClass('fieldLoading').attr("disabled","disabled");
            FB.login(loginCallback, {scope:'public_profile,pages_show_list', return_scopes: true});  
        });
    });
})


</script>
@endsection