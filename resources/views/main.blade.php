<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <style>
        .fieldLoading {    
        background-color: #ffffff;
        background-image: url("http://loadinggif.com/images/image-selection/3.gif");
        background-size: 25px 25px;
        background-position:right center;
        background-repeat: no-repeat;
        display:grid;
        }     
    </style>
</head>
    <body>
        
        <div class="container">
            <div class="row justify-content-center mt-5" >
                <div class="col-md-8" style="min-height:300px; background:#ddd; border: 1px solid #343a40; border-radius:5px;">
 
                    @yield('content')
         
                </div>
            </div>
            
        </div>

        <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <script src="{{ asset('app/js/custom.js') }}"></script>
        @yield('js')
    </body>
</html>