@extends('main')
@section('content')
<div class="row justify-content-center" style="margin-top:20px;">
    <div class="form-group">
        <select id="pagesPaginator" class="form-control">
            <option value="">choose page to get its posts</option>
            @foreach($data->pages as $page)
                <option value="{{ $page->id }}" token="{{ $page->access_token }}">{{ $page->name }}</option>
            @endforeach
        </select>
    </div>
    <button class="btn btn-primary" id="create-btn" data-toggle="modal" data-target="#createPostForm" disabled>create post</button>
</div>
<div class="row" id="postsContainer">
    
</div>
<form class="modal fade" id="createPostForm" tabindex="-1" role="dialog" aria-hidden="true" enctype="multipart/form-data">
  <input type="text" name="page_id" style="display:none;">
  <input type="text" name="page_token" style="display:none;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Create new post ()</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
            <textarea class="form-control" name="message" id="message" rows="3" placeholder="Post text goes here"></textarea>
        </div>           
        <div class="custom-file">
            <input type="file" class="custom-file-input" name="image" id="image" accept="image/*" required>
            <label class="custom-file-label" for="validatedCustomFile">Choose post image...</label>
            <div class="invalid-feedback"></div>
        </div>   
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>        
        <button type="submit" class="btn btn-primary">Post</button>
      </div>
    </div>
  </div>
</form>
@endsection
@section('js')
<script>
    var getPageDataRoute = "{{ route('getPageData') }}";
    var createPostRoute = "{{ route('createPost') }}";
</script>
@append