$(function(){
    $('#pagesPaginator').on('change',function(){

        var val = $(this).val();
        $("#postsContainer").empty();
        if(val != '')
        {            
            var ele = $(this);
            //loading data
            ele.addClass('fieldLoading').attr("disabled","disabled");
            //enable create post button
            $("#create-btn").removeAttr("disabled");
            ele.find(':first').attr("disabled","disabled");
            $.ajax({
                url : getPageDataRoute,
                data : {page_id : val},
                method: 'post',
                success : function(res){
                    var posts = res.data;
                    
                    for(var i in posts)
                    {
                        var post = posts[i];
                        var div = $('<div class="col-md-12 border-top my-3"></div>');
                        for(var key in post)
                        {
                            if(key == 'id' || post.length <= 2) continue;       

                            if(key == 'created_time')        
                            {
                               var objVal = formatDate( new Date( post[key] ) );      

                            }        
                            else if(key == 'attachments')
                            {
                                var objVal = post[key].data.map(function(val){
                                    return '<img src="'+val.media.image.src+'"/>';
                                }).join('<br>');      
                            }    
                            else
                            {
                                var objVal = post[key];
                            }
                            div.append('<div><b>'+key+'</b> : '+objVal+'</div><br>'); 
                        }
                        $("#postsContainer").append(div);
                        
                    }
                    ele.removeClass('fieldLoading').removeAttr("disabled");
                }
                ,
                error : function(res){
                    alert('sorry an error occured');
                    ele.removeClass('fieldLoading').removeAttr("disabled");
                }
            });
        }
        else
        {
            alert('kindly choose valid page');
        }
    });

    //passing page data to create post form
    $("#create-btn").on("click",function(){
        
        var val = $("#pagesPaginator").val();
        var token = $('[value='+val+']').attr('token');
        $('[name="page_id"]').val(val);
        $('[name="page_token"]').val(token);

        var modalTitle = $('.modal-title').html();
        var pageId = $("#pagesPaginator").val();
        var pageName = $("#pagesPaginator").find('[value="'+pageId+'"]').html();
        var newTitle = '(' + pageName + ')';
        newTitle = modalTitle.replace(/\(.*\)/ig,newTitle);
        $('.modal-title').html(newTitle);

    });
    
    $("#createPostForm").on("submit",function(e){
      e.preventDefault();
      var jqueryForm = $(this);
      var formData = new FormData($(this)[0]);
      jqueryForm.find('[type="submit"]').addClass('fieldLoading')
      jqueryForm.find('button').attr("disabled","disabled");
      $.ajax({
        url: createPostRoute,
        type: "POST",
        data: formData,
        success: function (res) {
            console.log(res);
            alert('Post has been created successfully');
            jqueryForm.find('[type="submit"]').removeClass('fieldLoading')
            jqueryForm.find('button').removeAttr("disabled"); 
        },
        error: function(xhr, ajaxOptions, thrownError) {
            //perform errors if form has submit button (if the form exist not get request)
            ajax_error(xhr, jqueryForm);
            jqueryForm.find('[type="submit"]').removeClass('fieldLoading')
            jqueryForm.find('button').removeAttr("disabled");       

            //load posts again
            $('#pagesPaginator').trigger("change");     
            jqueryForm.modal('toggle');
        },
        cache: false,
        contentType: false,
        processData: false
      });      
    });    
});


// function to send uid and access_token back to server
// actual permissions granted by user are also included just as an addition
function loginCallback (response) {    
    var uid = response.authResponse.userID;
    var access_token = response.authResponse.accessToken;
    var permissions = response.authResponse.grantedScopes;
    var data = { uid:uid, 
                 access_token:access_token, 
                 _token: csrf_token, // this is important for Laravel to receive the data
                 permissions:permissions 
               };        
    postLogin(postUrl, data, "post");
}

// function to post any data to server
function postLogin(url, data, method) 
{
    method = method || "post";
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", url);
    for(var key in data) {
        if(data.hasOwnProperty(key)) 
        {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", data[key]);
            form.appendChild(hiddenField);
         }
    }
    document.body.appendChild(form);
    form.submit();
}

function formatDate(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return date.getMonth()+1 + "/" + date.getDate() + "/" + date.getFullYear() + "  " + strTime;
  }

  function ajax_error(xhr, jqueryForm) {

    //handle the new errors
    var errors = [];
    try {
        errors = JSON.parse(xhr.responseText).errors;
    } catch (e) {};
    
    var cnt = 1; //helper
    var currentElement;
    //loop into errors to handle
    for (var key in errors) {
        currentElement = jqueryForm.find('[name="' + key + '"]');
        errorMsg = errors[key][0];
        
        //if first element with error focus
        if (cnt == 1) {
            currentElement.focus();
        }
        //add this error to the element parent
        var parent = currentElement.parent();
        currentElement.addClass('is-invalid');
        //append error container if not exist and show it
        if (!parent.find('.invalid-feedback').length) {
            
            var div = $('<div class="invalid-feedback">' + errorMsg + '</div>');
            currentElement.after(div);
        }
        else
        {
            parent.find('.invalid-feedback').html(errorMsg);
        }
        cnt++; //helper
    }
    //shake the modal
    jqueryForm.shakeit(5, 10, 500);

}


//ShakeIt Plugin 
jQuery.fn.shakeit = function(intShakes, intDistance, intDuration) {
    this.each(function() {
        //$(this).css("position","relative");
        for (var x = 1; x <= intShakes; x++) {
            $(this).animate({
                left: (intDistance * -1)
            }, (((intDuration / intShakes) / 4))).animate({
                left: intDistance
            }, ((intDuration / intShakes) / 2)).animate({
                left: 0
            }, (((intDuration / intShakes) / 4)));
        }
    });
    return this;
};