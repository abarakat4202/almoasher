<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::redirect('/','login');
Route::get('/login','FacebookController@getLogin')->name('getLogin');
Route::post('/login','FacebookController@getPages')->name('postLogin');
Route::post('/getPageData','FacebookController@getPageData')->name('getPageData');
Route::post('/createPost','FacebookController@createPost')->name('createPost');

